import React from 'react'
import Link from 'next/link'
import { IHomepage } from './types'

const Page: React.FC<IHomepage> = ({ content }) => (
  <div>
    <h1>Index page</h1>
    <h2>{content.title}</h2>
    {/* As in the documentation: https://nextjs.org/docs/api-reference/next/link */}
    <Link href="/notes">
      <a>Notes</a>
    </Link>
  </div>
)

export default Page

export function getStaticProps() {
  // Get CMS data

  return {
    props: {
      content: {
        id: 11,
        data: Date.now(),
        title: 'This is my really nice app',
      },
    },
  }
}
