import React from 'react'
import { useRouter } from 'next/router'

const NoteWithParams: React.FC = () => {
  const router = useRouter()
  const { params } = router.query

  // route: /notes/1/2/3/4
  // params will be an array of 4

  // if you want to include the parent as well, you can use a catch all route [[...params]].jsx

  return <div>Note with params: {JSON.stringify(params)}</div>
}

export default NoteWithParams
