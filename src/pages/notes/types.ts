type NoteT = {
  id: number
  date: number
  title: string
}

type NotesT = NoteT[]

interface INotes {
  notes: NotesT
}

interface INote {
  note: NoteT
}

export type { INote, INotes, NoteT, NotesT }
