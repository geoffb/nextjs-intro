import React from 'react'
import { INote } from './types'

const Note: React.FC<INote> = ({ note }) => {
  return <div>Note with id: {note.title}</div>
}

export default Note

export async function getServerSideProps({ params, req, res }) {
  const response = await fetch(`http://localhost:3000/api/notes/${params.id}`)

  if (!response.ok) {
    console.log(`Note not found! (with id: ${params.id})`)
    res.writeHead(302, {
      Location: '/notes',
    })

    res.end()

    return {
      props: {},
    }
  }

  const { data } = await response.json()

  return {
    props: { note: data },
  }
}
