import React, { useCallback } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
// https://nextjs.org/docs/basic-features/built-in-css-support#adding-component-level-css
import styles from 'css/notes.module.css'
import { INotes } from './types'

const Notes: React.FC<INotes> = ({ notes }) => {
  const router = useRouter()
  const onClick = useCallback(() => router.push('/'), []) // .push('/notes/[id]', '/notes/1')

  return (
    <div>
      <h1>Notes index</h1>
      {notes.map((note) => (
        <React.Fragment key={note.id}>
          <Link href="/notes/[id]" as={`/notes/${note.id}`}>
            <a className={styles.noteLink}>{note.title}</a>
          </Link>
          <br />
        </React.Fragment>
      ))}
      <Link href="/notes/[...params]" as="/notes/1/with/params">
        <a className={styles.noteLink}>Note with params</a>
      </Link>
      <br />
      <Link href="/">
        <a className={styles.noteLink}>Index</a>
      </Link>

      <br />
      <br />
      <br />
      <button onClick={onClick}>Click me to use an onClick router push</button>
    </div>
  )
}

export default Notes

// Only runs on the server
export async function getServerSideProps() {
  const res = await fetch('http://localhost:3000/api/notes')
  const { data } = await res.json()

  return {
    props: { notes: data },
  }
}
