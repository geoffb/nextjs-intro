// https://www.npmjs.com/package/next-connect
import type { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from "next-connect"

const router = createRouter<NextApiRequest, NextApiResponse>()

router
    .get((req, res) => {
        res.send("Hello get");
    })
    .post(async (req, res) => {
        res.json({message: "hello post"});
    })

export default router.handler({
    onError: (err: any, req:NextApiRequest , res: any) => {
        console.error(err.stack)
        res.status(500).end("Something broke!")
    },
    onNoMatch: (req, res) => {
        res.status(404).end("Page is not found")
    },
})

// export default (req, res) => {
//   res.statusCode = 200
//   res.setHeader('Content-Type', 'application/json')
//   res.end(JSON.stringify({ message: 'hello' }))
// }
