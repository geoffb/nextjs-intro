import type { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from "next-connect"
import notes from 'data/data'

const router = createRouter<NextApiRequest, NextApiResponse>()

router
  .get((req, res) => {
    res.json({data: notes})
  })
  .post(async (req, res) => {
    const id = Date.now()
    const note = {...req.body, id}

    notes.push(note)
    res.json({data: note})
  })

export default router.handler({
  onError: (err: any, req:NextApiRequest , res: any) => {
    console.error(err.stack)
    res.status(500).end("Something broke!")
  },
  onNoMatch: (req, res) => {
    res.status(404).end("Page is not found")
  },
})
