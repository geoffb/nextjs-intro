import type { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from "next-connect"
import notes from 'data/data'

const router = createRouter<NextApiRequest, NextApiResponse>()
const getNote = id => notes.find(n => n.id === parseInt(id))

router
  .get((req, res) => {
    const note = getNote(req.query.id)
    console.log("Searching", note, notes)
    if (!note) {
      res.status(404)
      res.end()
      return
    }

    res.json({data: note})
  })
  .patch((req, res) => {
    const note = getNote(req.query.id)

    if (!note) {
      res.status(404)
      res.end()
      return
    }

    const i = notes.findIndex(n => n.id === parseInt(<string>req.query.id))
    const updated = {...note, ...req.body}

    notes[i] = updated
    res.json({data: updated})
  })
  .delete((req, res) => {
    const note = getNote(req.query.id)

    if (!note) {
      res.status(404)
      res.end()
      return
    }
    const i = notes.findIndex(n => n.id === parseInt(<string>req.query.id))

    notes.splice(i, 1)

    res.json({data: req.query.id})
  })


export default router.handler({
  onError: (err: any, req:NextApiRequest , res: any) => {
    console.error(err.stack)
    res.status(500).end("Something broke!")
  },
  onNoMatch: (req, res) => {
    res.status(404).end("Page is not found")
  },
})
