type ContentT = {
  date: number
  id: number
  title: string
}

interface IHomepage {
  content: ContentT
}

export type { ContentT, IHomepage }
