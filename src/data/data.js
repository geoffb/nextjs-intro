const notes = new Array(15)
    .fill(1)
    .map((_, i) => ({
        id: i,
        date: Date.now(),
        title: `Note ${i}`
    }))

module.exports = notes
