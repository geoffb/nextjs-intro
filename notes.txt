$ yarn add next react react-dom

"scripts": {
"dev": "next",
"build": "next build",
"start": "next start"
}

create a page directory at the root and a index.tsx

yarn add --dev typescript @types/react @types/node

$ yarn next

http://localhost:3000/

Notes: https://hendrixer.github.io/nextjs-course/pages

Non pages, for all non pages components, the community usually 
creates a /src/components folder
It's possible to also move the pages directory inside of the src 
directory, it will work just fine (need to restart the app)
_*.jsx will make the file not a route in pages

pages/_app is where you can import global styling:

Otherwise you get this error: Global CSS cannot be imported from 
files other than your Custom <App>. Due to the Global nature of 
stylesheets, and to avoid conflicts, Please move all first-party 
global CSS imports to pages/_app.js. Or convert the import to 
Component-Level CSS (CSS Modules).
Read more: https://nextjs.org/docs/messages/css-global

--------
import '../src/css/global.css'

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

If this doesn't work just like that. Remove the .next directory containing
the cache and static files
--------

To add css to other components it need to be a module
styles.module.css

Theme UI has presets - roboto, tailwind
and has components 

Similar: BaseWeb (Uber) - https://baseweb.design/

--------

Customizing Next.js config -> next.config.js (root)
Can be an object or a function
https://nextjs.org/docs/api-reference/next.config.js/introduction
    - Webpack conf
    - Env vars

const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    console.log('DEV ENVIRONMENT CONFIG')
  }

  return defaultConfig
}

--------

Plugins
https://nextjs.org/examples
https://hendrixer.github.io/nextjs-course/plugins

--------
All of these methods are for prerendering Pages only.

getStaticProps:
    export async function getStaticProps(context) {
      return {
        props: {}
      }
    }

    Next.js will run this function at build time.
    Whatever your return as props will be passed into the exported page.

getStaticPaths:
    export async function getStaticPaths() {
      // get all the paths for your posts from an API
      // or file system
      const results = await fetch('/api/posts')
      const posts = await results.json()
      const paths = posts.map(post => ({params: {slug:
      post.slug}}))
      /*
      [
        {params: {slug: 'get-started-with-node'}},
        {params: {slug: 'top-frameworks'}}
      ]
      */
      return {paths}
    }

    export async function getStaticProps({ params }) {
      const res = await fetch(`/api/post/${params.slug}`)
      const post = await res.json()
      return {
        props: {post}
      }
    }

    If a page has a dynamic path [id].jsx and uses getStaticProps,
    it must also use getStaticPaths to prerender all the pages at build time into HTML.

getServerSideProps:
    export async function getServerSideProps() {
      const response = await fetch(`https://somedata.com`)
      const data = await response.json()

      return { props: { data } }
    }

    This will be called at runtime during every request.

When to use what
- Do you need data at runtime but don't need SSR? Use client-side data fetching.
- Do you need data at runtime but do need SSR? Use getServerSideProps
- Do you have pages that rely on data that is cachable and accessible at build time? Like from a CMS? Use getStaticProps
- Do you have the same as above but the pages have dynamic URL params? Use getStaticProps and getStaticPaths

---------

Sometimes you just need to skip rendering some component on the server because:

it depends on the DOM API
it depends on client-side data
something else
Next.js supports dynamic imports that, when used with components, will opt out of SSR.

import dynamic from 'next/dynamic'

const SponsoredAd = dynamic(
  () => import('../components/sponsoredAd'),
  { ssr: false }
)

const Page = () => (
  <div>
    <h1>This will be prerendered</h1>

    {/* this won't*/}
    <SponsoredAd />
  </div>
)

export default Page

